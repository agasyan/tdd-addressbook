from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from .base import FunctionalTest

class DeleteContactItemTest(FunctionalTest):  
    def test_can_delete_list_item(self):
        self.browser.get(self.live_server_url)

        # User mengisi item pertama
        inputbox_name = self.browser.find_element_by_id('id_new_name')  
        self.assertEqual(
            inputbox_name.get_attribute('placeholder'),
            'Enter a contact name'
        )

        inputbox_phone = self.browser.find_element_by_id('id_new_phone')  
        self.assertEqual(
            inputbox_phone.get_attribute('placeholder'),
            'Enter a contact phone'
        )

        inputbox_address = self.browser.find_element_by_id('id_new_address')  
        self.assertEqual(
            inputbox_address.get_attribute('placeholder'),
            'Enter a contact address'
        )

        # Mengisi Input
        inputbox_name.send_keys('Budi')  
        inputbox_phone.send_keys('14045')  
        inputbox_address.send_keys('Jl. America No.2')  

        # Submit new contact
        self.browser.execute_script("document.getElementById('submit-button').click();")
        self.wait_for_row_in_list_table('Budi')

        # User mengisi item kedua
        inputbox_name = self.browser.find_element_by_id('id_new_name')  
        self.assertEqual(
            inputbox_name.get_attribute('placeholder'),
            'Enter a contact name'
        )

        inputbox_phone = self.browser.find_element_by_id('id_new_phone')  
        self.assertEqual(
            inputbox_phone.get_attribute('placeholder'),
            'Enter a contact phone'
        )

        inputbox_address = self.browser.find_element_by_id('id_new_address')  
        self.assertEqual(
            inputbox_address.get_attribute('placeholder'),
            'Enter a contact address'
        )

        # Mengisi Input
        inputbox_name.send_keys('Po')  
        inputbox_phone.send_keys('14022')  
        inputbox_address.send_keys('Jl. America No.1')  

        # Submit new contact
        self.browser.execute_script("document.getElementById('submit-button').click();")
        self.wait_for_row_in_list_table('Po')

        
        # Karena user sudah musuhan sama budi dihapus
        self.browser.execute_script("document.getElementById('del-button-1').click();")

        # Mengecek kfc sudah tidak ada di to do list
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Budi', page_text)

    
