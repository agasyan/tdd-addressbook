from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class NewVisitorTest(FunctionalTest):  
    def test_create_new_contact(self):  
        self.browser.get(self.live_server_url)

        inputbox_name = self.browser.find_element_by_id('id_new_name')  
        self.assertEqual(
            inputbox_name.get_attribute('placeholder'),
            'Enter a contact name'
        )

        inputbox_phone = self.browser.find_element_by_id('id_new_phone')  
        self.assertEqual(
            inputbox_phone.get_attribute('placeholder'),
            'Enter a contact phone'
        )

        inputbox_address = self.browser.find_element_by_id('id_new_address')  
        self.assertEqual(
            inputbox_address.get_attribute('placeholder'),
            'Enter a contact address'
        )

        # Mengisi Input
        inputbox_name.send_keys('Budi')  
        inputbox_phone.send_keys('14045')  
        inputbox_address.send_keys('Jl. America No.2')  

        # Submit new contact
        self.browser.execute_script("document.getElementById('submit-button').click();")

        # Mengecek apakah ada entry budi
        self.wait_for_row_in_list_table('Budi')
