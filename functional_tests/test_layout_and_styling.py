from selenium.webdriver.common.keys import Keys
from .base import FunctionalTest

class LayoutAndStylingTest(FunctionalTest):   
    def test_header_and_title(self):  
        self.browser.get(self.live_server_url)

        self.assertIn('Address-Book', self.browser.title)

        header_text = self.browser.find_element_by_tag_name('h1').text  
        self.assertIn('Add New Contact', header_text)
    
    def test_layout_and_styling(self):
        # Edith goes to the home page
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        inputbox_name = self.browser.find_element_by_id('id_new_name')
        self.assertAlmostEqual(
            inputbox_name.location['x'] + inputbox_name.size['width'] / 2,
            512,
            delta=10
        )

