from selenium.webdriver.common.keys import Keys
from .base import FunctionalTest

class ItemValidationTest(FunctionalTest):
    def test_cannot_add_empty_list_items(self):
        def test_cannot_add_empty_list_items(self):
            # Langsung Submit tidak bisa
            self.browser.get(self.live_server_url)
            self.browser.execute_script("document.getElementById('submit-button').click();")

            # The home page refreshes, and there is an error message saying
            # that list items cannot be blank

            inputbox_name = self.browser.find_element_by_id('id_new_name')  
            self.assertEqual(
                inputbox_name.get_attribute('placeholder'),
                'Enter a contact name'
            )

            inputbox_phone = self.browser.find_element_by_id('id_new_phone')  
            self.assertEqual(
                inputbox_phone.get_attribute('placeholder'),
                'Enter a contact phone'
            )

            inputbox_address = self.browser.find_element_by_id('id_new_address')  
            self.assertEqual(
                inputbox_address.get_attribute('placeholder'),
                'Enter a contact address'
            )

            # Mengisi Input
            inputbox_name.send_keys('Budi')  
            inputbox_phone.send_keys('14045')  
            inputbox_address.send_keys('Jl. America No.2')  

            self.wait_for_row_in_list_table('Budi')
