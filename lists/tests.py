from django.test import TestCase
from lists.models import Contact


class HomePageTest(TestCase):

    def test_uses_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_can_save_a_POST_request(self):
        response = self.client.post('/', data={'contact_name': 'Budi','contact_phone':'14045', 'contact_address':'depok'})

        self.assertEqual(Contact.objects.count(), 1)  
        new_item = Contact.objects.first()  
        self.assertEqual(new_item.name, 'Budi')  

        self.assertIn('Budi', response.content.decode())
        self.assertTemplateUsed(response, 'home.html')
    
    def test_displays_all_list_items(self):
        Contact.objects.create(name='budi 1', phone='14022', address='depok')
        Contact.objects.create(name='budi 2', phone='14021', address='bogor')

        response = self.client.get('/')

        self.assertIn('budi 1', response.content.decode())
        self.assertIn('budi 2', response.content.decode())

class ItemModelTest(TestCase):

    def test_saving_and_retrieving_items(self):
        first_item = Contact()
        first_item.name = 'Budi Pratama'
        first_item.phone = '14045'
        first_item.address = 'Jl. Margonda Raya No.525'
        first_item.save()

        second_item = Contact()
        second_item.name = 'Budi Dwi'
        second_item.phone = '14022'
        second_item.address = 'Jl. Margonda Raya No.225'
        second_item.save()

        saved_items = Contact.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.name, 'Budi Pratama')
        self.assertEqual(second_saved_item.name, 'Budi Dwi')
    
    def test_saving_and_deleting_items(self):
        first_item = Contact()
        first_item.name = 'Budi Pratama'
        first_item.phone = '14045'
        first_item.address = 'Jl. Margonda Raya No.525'
        first_item.save()

        saved_items = Contact.objects.all()
        self.assertEqual(saved_items.count(), 1)

        saved_obj = saved_items[0]

        saved_obj.delete()       

        saved_items_after_del = Contact.objects.all()
        self.assertEqual(saved_items_after_del.count(), 0)

class DeleteItemTest(TestCase):
    
    def test_delete_todo(self):

        # Crete Contact
        newContact1 = Contact.objects.create(name='budi 1', phone='14022', address='depok')
        newContact2 = Contact.objects.create(name='budi 2', phone='14021', address='bogor')

        self.client.get(
            f'/delete_item/{newContact1.id}/'
        )

        self.assertEqual(Contact.objects.all().count(), 1)

    def test_delete_redirects_to_list_view(self):
        # Crete Contact
        newContact1 = Contact.objects.create(name='budi 1', phone='14022', address='depok')
        newContact2 = Contact.objects.create(name='budi 2', phone='14021', address='bogor')

        response = self.client.get(
            f'/delete_item/{newContact1.id}/'
        )

        self.assertRedirects(response, f'/')