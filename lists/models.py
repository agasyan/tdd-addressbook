from django.db import models

class Contact(models.Model):
    name = models.TextField()
    phone = models.TextField()
    address = models.TextField()