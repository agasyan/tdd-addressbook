from django.shortcuts import render, redirect
from django.http import HttpResponse
from lists.models import Contact

def home_page(request):
    if request.method == 'POST':
        new_contact_name = request.POST['contact_name'] 
        new_contact_phone = request.POST['contact_phone']  
        new_contact_address = request.POST['contact_address']  
        Contact.objects.create(name=new_contact_name, phone=new_contact_phone, address=new_contact_address)

    items = Contact.objects.all()
    return render(request, 'home.html', {'items': items})

def delete_contact(request, contact_id):
    contact = Contact.objects.get(id=contact_id)
    contact.delete()
    return redirect(f'/')