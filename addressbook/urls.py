from django.conf.urls import url
from lists import views

urlpatterns = [
    url(r'^$', views.home_page, name='home'),
    url(r'^delete_item/(?P<contact_id>\d+)/$', views.delete_contact, name='delete_contact'),
]
